package fule.com.kotlindemo.activity

import android.support.v7.widget.LinearLayoutManager
import android.view.View
import fule.com.kotlindemo.R
import fule.com.kotlindemo.adapter.NotifyAdapter
import fule.com.kotlindemo.base.BaseActivity
import fule.com.kotlindemo.base.BasePresenter
import fule.com.kotlindemo.base.BaseView
import fule.com.kotlindemo.model.NotifyModel
import fule.com.kotlindemo.weight.DividerDecoration
import kotlinx.android.synthetic.main.activity_base_toolbar_dark.*
import kotlinx.android.synthetic.main.activity_nofity.*
import java.util.*

/**
 * 作者： njb
 * 时间： 2018/8/8 0008-下午 4:21
 * 描述： 通知
 * 来源：
 */
class NotifyActivity : BaseActivity<BasePresenter<BaseView>>(), View.OnClickListener {


    private var notifyAdapter: NotifyAdapter? = null
    private val modelList = ArrayList<NotifyModel>()
    private var model: NotifyModel? = null

    override val layoutId: Int
        get() = R.layout.activity_nofity

    override fun createPresenter(): BasePresenter<BaseView> {
        return BasePresenter(this)
    }

    override fun initView() {
        toolbar.title = "通知"
        model = NotifyModel()
        for (i in 0..4) {
            model!!.time = "2018-06-14 11:00"
            model!!.type = "公务模块"
            model!!.title = "校友会学习模块"
            model!!.look = "99"
            modelList.add(model!!)
        }
        notifyAdapter = NotifyAdapter(modelList)
        rv_notify!!.layoutManager = LinearLayoutManager(this)
        rv_notify!!.addItemDecoration(DividerDecoration(context.resources.getColor(R.color.ce7e7e7), 1))
        rv_notify!!.adapter = notifyAdapter
    }

    override fun addListener() {
        toolbar.setOnClickListener(this)
        notifyAdapter!!.setOnItemChildClickListener { adapter, view, position ->
            when (view.id) {
                R.id.tv_edit -> showtoast("这是查看")
                R.id.tv_delete -> showtoast("这是删除")
            }
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.toolbar ->
                onBackPressed()
        }
    }
}
