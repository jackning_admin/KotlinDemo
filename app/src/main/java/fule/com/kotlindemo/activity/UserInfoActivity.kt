package fule.com.kotlindemo.activity

import android.annotation.SuppressLint
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.view.View
import fule.com.kotlindemo.R
import fule.com.kotlindemo.adapter.ViewPagerAdapter
import fule.com.kotlindemo.base.BaseActivity
import fule.com.kotlindemo.base.BasePresenter
import fule.com.kotlindemo.base.BaseView
import fule.com.kotlindemo.fragment.*
import kotlinx.android.synthetic.main.activity_base_toolbar_dark.*
import kotlinx.android.synthetic.main.activity_user_info.*
import kotlinx.android.synthetic.main.fm_video.*

/**
 * 作者： njb
 * 时间： 2018/7/27 0027-下午 3:35
 * 描述： 用户信息
 * 来源：
 */
class UserInfoActivity : BaseActivity<BasePresenter<BaseView>>(), View.OnClickListener {
    private val recommendFragment = UserInfoFragment()
    private val followFragment = LiveFragment()
    private val productFragment = VideoFragment()
    private val activeFragment = CircleFragment()
    private var list: MutableList<Fragment>? = null

    override val layoutId: Int
        get() = R.layout.activity_user_info

    override fun createPresenter(): BasePresenter<BaseView> {
        return BasePresenter(this)
    }

    @SuppressLint("SetTextI18n")
    override fun initView() {
        titleTv.text = "用户主页"
        userNameTv.text = "红太狼老公"
        numberTv.text = "乐创号: 100689"
        levelNameTv.text = "女主"
        joinLevelTv.text = "创业合伙人"
        masterLeverTv.text = "资深讲师"
        focusCb.text = "粉丝55"
        blacklistCb.text = "关注19"
        initFragment()
    }

    override fun addListener() {
        toolbar.setOnClickListener(this)

    }

    private fun initFragment() {
        list = ArrayList()
        list!!.add(recommendFragment)
        list!!.add(followFragment)
        list!!.add(productFragment)
        list!!.add(activeFragment)


    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.toolbar ->
                onBackPressed()
        }
    }

}