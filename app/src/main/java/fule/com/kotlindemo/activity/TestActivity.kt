package fule.com.kotlindemo.activity

import android.content.Intent
import fule.com.kotlindemo.R
import fule.com.kotlindemo.adapter.LiveAdapter
import fule.com.kotlindemo.base.BaseActivity
import fule.com.kotlindemo.base.BasePresenter
import fule.com.kotlindemo.base.BaseView
import fule.com.kotlindemo.model.Person

/**
 * 作者： njb
 * 时间： 2018/7/25 0025-上午 11:18
 * 描述： 测试
 * 来源：
 */

class TestActivity : BaseActivity<BasePresenter<BaseView>>() {
    private val liveAdapter: LiveAdapter? = null
    override val layoutId: Int
        get() = R.layout.fm_live

    override fun createPresenter(): BasePresenter<BaseView> {
        return BasePresenter(this)
    }

    override fun initView() {
        val person = Person()
        person.age = 18
        person.name = "Tom"
        val intent = Intent(this@TestActivity, SecondActivity::class.java)
/*        intent.putExtra("person_data", person)
        intent.putParcelableArrayListExtra("person_data",person)
        startActivity(intent)*/
        liveAdapter!!.setOnItemClickListener { adapter, view, position -> startActivity(Intent(this@TestActivity, LiveActivity::class.java)) }
    }

    override fun addListener() {

    }
}


