package fule.com.kotlindemo.activity

import android.support.design.widget.BottomNavigationView
import fule.com.kotlindemo.R
import fule.com.kotlindemo.base.BaseActivity
import fule.com.kotlindemo.base.BasePresenter
import fule.com.kotlindemo.base.BaseView
import fule.com.kotlindemo.fragment.*
import kotlinx.android.synthetic.main.activity_main.*

/**
 * 作者： njb
 * 时间： 2018/7/16 0016-上午 10:03
 * 描述：
 * 来源：
 */
class MainActivity : BaseActivity<BasePresenter<BaseView>>(), HomeFragment.OnFragmentInteractionListener{
    override fun onFragmentInteraction() {

    }

    private val fragmentHome = HomeFragment.newInstance()
    private val fragmentLive = VideoFragment.newInstance()
    private val fragmentCircle = CircleFragment.newInstance()
    private val fragmentMine = MineFragment.newInstance()

    override fun initView() {
        addFragmentToActivity(supportFragmentManager, fragmentHome, R.id.frameLayout)
    }

    override val layoutId: Int get() = R.layout.activity_main



    override fun createPresenter(): BasePresenter<BaseView> {
        return BasePresenter(this)
    }


    override fun addListener() {
        navigation!!.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                addFragmentToActivity(supportFragmentManager, fragmentHome, R.id.frameLayout)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_video -> {
                addFragmentToActivity(supportFragmentManager, fragmentLive, R.id.frameLayout)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_circle -> {
                addFragmentToActivity(supportFragmentManager, fragmentCircle, R.id.frameLayout)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_mine -> {
                addFragmentToActivity(supportFragmentManager,fragmentMine,R.id.frameLayout)
                return@OnNavigationItemSelectedListener true
            }

        }
        false
    }
}