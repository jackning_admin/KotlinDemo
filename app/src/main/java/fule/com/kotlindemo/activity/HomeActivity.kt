package fule.com.kotlindemo.activity

import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.util.TypedValue
import android.widget.FrameLayout
import android.widget.TextView
import fule.com.kotlindemo.R
import fule.com.kotlindemo.adapter.FragmentAdapter
import fule.com.kotlindemo.adapter.MainPagerAdapter
import fule.com.kotlindemo.base.BaseActivity
import fule.com.kotlindemo.base.BasePresenter
import fule.com.kotlindemo.base.BaseView
import fule.com.kotlindemo.fragment.MineFragment
import fule.com.kotlindemo.util.Utils
import java.util.ArrayList

/**
 * 作者： njb
 * 时间： 2018/7/18 0018-上午 9:20
 * 描述：
 * 来源：
 */
class HomeActivity : BaseActivity<BasePresenter<BaseView>>() {

    private var tabTablayout: TabLayout? = null
    private var viewPager: ViewPager? = null
    private var flContains: FrameLayout? = null
    private var one: TabLayout.Tab? = null
    private var two: TabLayout.Tab? = null
    private var three: TabLayout.Tab? = null
    private var four: TabLayout.Tab? = null
    private var adapter: MainPagerAdapter? = null

    override fun initView() {
        tabTablayout = findViewById(R.id.tablayout)
        flContains = findViewById(R.id.fl_contains)
        viewPager = findViewById(R.id.viewPager)
        one = tabTablayout!!.getTabAt(0)
        two = tabTablayout!!.getTabAt(1)
        three = tabTablayout!!.getTabAt(2)
        four = tabTablayout!!.getTabAt(3)
        initTab()
    }

    override fun createPresenter(): BasePresenter<BaseView> {
        return BasePresenter(this)
    }

    override val layoutId: Int get() = R.layout.activity_home

    override fun addListener() {
    }

    private fun initTab() {
        adapter = MainPagerAdapter(supportFragmentManager)
        for (i in 0 until adapter!!.count) {
            tabTablayout!!.addTab(tabTablayout!!.newTab().setCustomView(adapter!!.getTabView(this, i)))
        }
        tabTablayout!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                updateTab(tab, 14)
                val position = tab.position
                onTabSelect(position)
                viewPager!!.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
                updateTab(tab, 12)
            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })
        onTabSelect(0)
        updateTab(tabTablayout!!.getTabAt(0)!!, 14)
    }

    private fun updateTab(tab: TabLayout.Tab, sizeSp: Int) {
        val textView = Utils.getView<TextView>(tab.customView!!, R.id.tab_title)
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, sizeSp.toFloat())
    }

    private fun onTabSelect(position: Int) {
        val fragment = adapter!!.instantiateItem(flContains!!, position) as Fragment
        adapter!!.setPrimaryItem(flContains!!, position, fragment)
        adapter!!.finishUpdate(flContains!!)
        adapter!!.destroyItem(flContains!!, position, fragment)
        title = adapter!!.getPageTitle(position)
    }

}