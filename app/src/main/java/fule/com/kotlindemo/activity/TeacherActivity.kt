package fule.com.kotlindemo.activity

import android.support.v7.widget.LinearLayoutManager
import android.view.View
import fule.com.kotlindemo.R
import fule.com.kotlindemo.adapter.PublicAdapter
import fule.com.kotlindemo.adapter.TeacherAdapter
import fule.com.kotlindemo.base.BaseActivity
import fule.com.kotlindemo.base.BasePresenter
import fule.com.kotlindemo.base.BaseView
import fule.com.kotlindemo.model.TeacherModel
import fule.com.kotlindemo.weight.DividerDecoration
import kotlinx.android.synthetic.main.activity_base_toolbar_dark.*
import kotlinx.android.synthetic.main.fm_class.*

/**
 * 作者： njb
 * 时间： 2018/8/3 0003-下午 2:52
 * 描述： 讲师主页
 * 来源：
 */
class TeacherActivity : BaseActivity<BasePresenter<BaseView>>(),View.OnClickListener{
    private var teacherAdapter = TeacherAdapter(null)
    private var data = ArrayList<TeacherModel>()
    private var teacherModel = TeacherModel()

    override val layoutId: Int
        get() = R.layout.fm_class

    override fun createPresenter(): BasePresenter<BaseView> {
        return BasePresenter(this)
    }

    override fun initView() {
        toolbar.title = "名师"
        initAdapter()
    }

    private fun initAdapter() {
        for(i in 1..4){
            teacherModel.content = "名医面对面会诊接口各种疑难杂病"
            teacherModel.name = "叶云天"
            teacherModel.level = "高级董事"
            data.add(teacherModel)
        }
        teacherAdapter = TeacherAdapter(data)
        val manager = LinearLayoutManager(context)
        rv_class!!.addItemDecoration(DividerDecoration(context.resources.getColor(R.color.ce7e7e7), 1))
        rv_class.layoutManager = manager
        rv_class.adapter = teacherAdapter
    }

    override fun addListener() {
        toolbar.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.toolbar ->
                onBackPressed()
        }
    }

}