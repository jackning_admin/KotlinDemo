package fule.com.kotlindemo.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.Toast
import fule.com.kotlindemo.R
import fule.com.kotlindemo.adapter.MyAdapter
import fule.com.kotlindemo.view.OnItemClick
import fule.com.kotlindemo.view.SimpleDividerDecoration

/**
 * 作者： njb
 * 时间： 2018/7/17 0017-下午 5:59
 * 描述：
 * 来源：
 */
class KotlinActivity : AppCompatActivity(), OnItemClick {
    private var  data = ArrayList<String>()
    private var adapter: MyAdapter? = null
    private var recyclerView: RecyclerView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)
        initView()
        initAdapter()
    }

    private fun initView() {
        recyclerView = findViewById(R.id.rv)
    }

    private fun initAdapter() {
        adapter =  MyAdapter()
        adapter!!.add(getData())
        val  manager = LinearLayoutManager(this)
        recyclerView!!.layoutManager = manager
        recyclerView!!.addItemDecoration(SimpleDividerDecoration(this))
        recyclerView!!.adapter = adapter
        adapter!!.setItemClick(this)
    }

    private fun getData():List<String>{
        for(i in 1..100){
            this.data!!.add("我是标题 $i")
        }
        return data
    }

    override fun OnItemClick(view: View, position: Int) {
        Toast.makeText(this,"我是第" + position + "个", Toast.LENGTH_SHORT).show()
        goIntent()
    }

    private fun goIntent() {
        val intent  = Intent()
        intent.setClass(this,PercentActivity::class.java)
        startActivity(intent)
    }
}