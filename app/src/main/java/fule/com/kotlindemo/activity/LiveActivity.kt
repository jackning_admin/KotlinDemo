package fule.com.kotlindemo.activity

import fule.com.kotlindemo.R
import fule.com.kotlindemo.base.BaseActivity
import fule.com.kotlindemo.base.BasePresenter
import fule.com.kotlindemo.base.BaseView

/**
 * 作者： njb
 * 时间： 2018/7/20 0020-下午 2:16
 * 描述： 活动
 * 来源：
 */
class LiveActivity : BaseActivity<BasePresenter<BaseView>>() {
    override val layoutId: Int get() = R.layout.activity_home

    override fun createPresenter(): BasePresenter<BaseView> {
        return BasePresenter(this)
    }



    override fun initView() {}

    override fun addListener() {

    }


}
