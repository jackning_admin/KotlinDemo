package fule.com.kotlindemo.activity

import android.support.v7.widget.LinearLayoutManager
import android.view.View
import fule.com.kotlindemo.R
import fule.com.kotlindemo.adapter.PublicAdapter
import fule.com.kotlindemo.base.BaseActivity
import fule.com.kotlindemo.base.BasePresenter
import fule.com.kotlindemo.base.BaseView
import fule.com.kotlindemo.model.PublicModel
import fule.com.kotlindemo.weight.DividerDecoration
import kotlinx.android.synthetic.main.activity_base_toolbar_dark.*
import kotlinx.android.synthetic.main.fm_class.*

/**
 * 作者： njb
 * 时间： 2018/8/3 0003-下午 2:48
 * 描述： 公开课
 * 来源：
 */
class PublicClassActivity : BaseActivity<BasePresenter<BaseView>>(),View.OnClickListener{


    private var publicAdapter = PublicAdapter(null)
    private var data = ArrayList<PublicModel>()
    private var publicModel = PublicModel()

    override val layoutId: Int
        get() = R.layout.fm_class

    override fun createPresenter(): BasePresenter<BaseView> {
        return BasePresenter(this)
    }

    override fun initView() {
        toolbar.title = "公开课"
        initAdapter()
    }

    private fun initAdapter() {
        for(i in 1..4){
            publicModel.title = "名医面对面会诊接口各种疑难杂病"
            publicModel.looknumber = "观看205次"
            publicModel.status = "免费"
            data.add(publicModel)
        }
        publicAdapter = PublicAdapter(data)
        val manager = LinearLayoutManager(context)
        rv_class!!.addItemDecoration(DividerDecoration(context.resources.getColor(R.color.ce7e7e7), 1))
        rv_class.layoutManager = manager
        rv_class.adapter = publicAdapter
    }

    override fun addListener() {
        toolbar.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.toolbar ->
                    onBackPressed()
        }
    }

}