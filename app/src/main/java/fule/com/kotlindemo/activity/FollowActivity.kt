package fule.com.kotlindemo.activity

import android.support.v7.widget.LinearLayoutManager
import android.view.View
import fule.com.kotlindemo.R
import fule.com.kotlindemo.adapter.FollowAdapter
import fule.com.kotlindemo.base.BaseActivity
import fule.com.kotlindemo.base.BasePresenter
import fule.com.kotlindemo.base.BaseView
import fule.com.kotlindemo.model.FollowModel
import fule.com.kotlindemo.weight.DividerDecoration
import kotlinx.android.synthetic.main.activity_base_toolbar_dark.*
import kotlinx.android.synthetic.main.activity_follow.*

/**
 * 作者： njb
 * 时间： 2018/7/27 0027-下午 4:40
 * 描述：
 * 来源：
 */
class FollowActivity: BaseActivity<BasePresenter<BaseView>>(),View.OnClickListener{
    private var  followAdapter:FollowAdapter? = null
    private var list = ArrayList<FollowModel>()
    private var followModel:FollowModel? = null

    override val layoutId: Int
        get() = R.layout.activity_follow

    override fun createPresenter(): BasePresenter<BaseView> {
        return BasePresenter(this)
    }

    override fun initView() {
        toolbar.title = "我的关注"
        initAdapter()
    }

    private fun initAdapter() {
        followModel = FollowModel()
        for(i in 0..5){
            followModel!!.user_img = ""
            followModel!!.user_name = "红太狼"
            followModel!!.works = "作品17"
            followModel!!.fan = "粉丝55"
            list.add(followModel!!)
        }
        followAdapter = FollowAdapter(list)
        val manager = LinearLayoutManager(this)
        rv_follow!!.addItemDecoration(DividerDecoration(context.resources.getColor(R.color.ce7e7e7), 1))
        rv_follow!!.layoutManager= manager
        rv_follow!!.adapter = followAdapter

    }

    override fun addListener() {
        toolbar.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.toolbar ->
                onBackPressed()
        }
    }
}