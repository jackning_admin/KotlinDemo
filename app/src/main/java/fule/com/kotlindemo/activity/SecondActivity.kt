package fule.com.kotlindemo.activity

import android.util.Log
import fule.com.kotlindemo.R
import fule.com.kotlindemo.base.BaseActivity
import fule.com.kotlindemo.base.BasePresenter
import fule.com.kotlindemo.base.BaseView
import fule.com.kotlindemo.model.Person

/**
 * 作者： njb
 * 时间： 2018/8/14 0014-上午 10:31
 * 描述：
 * 来源：
 */
class SecondActivity : BaseActivity<BasePresenter<BaseView>>() {
    var TAG = "SecondActivity"
    private var person:Person? = null

    override val layoutId: Int
        get() = R.layout.fm_live

    override fun createPresenter(): BasePresenter<BaseView> {
        return BasePresenter(this)
    }

    override fun initView() {
        intent.extras.let {
            person = intent.getSerializableExtra("person_data") as Person?
        }
        Log.d(TAG, "person name is " + person!!.name)
        Log.d(TAG, "person age is " + person!!.age)
    }


    override fun addListener() {

    }
}
