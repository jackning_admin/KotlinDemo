package fule.com.kotlindemo.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import fule.com.kotlindemo.R
import fule.com.kotlindemo.model.PublicModel

/**
 * 作者： njb
 * 时间： 2018/8/3 0003-下午 12:38
 * 描述： 公开课
 * 来源：
 */
class PublicAdapter(list: List<PublicModel>?): BaseQuickAdapter<PublicModel,BaseViewHolder>(R.layout.item_public,list){
    override fun convert(helper: BaseViewHolder, item: PublicModel) {
        helper.setText(R.id.tv_title,item.title)
        helper.setText(R.id.tv_class_user_name,item.username)
        helper.setText(R.id.tv_look,item.looknumber)
    }

}