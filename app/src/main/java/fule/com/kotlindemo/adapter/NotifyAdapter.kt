package fule.com.kotlindemo.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder

import fule.com.kotlindemo.R
import fule.com.kotlindemo.model.NotifyModel

/**
 * 作者： njb
 * 时间： 2018/8/8 0008-下午 4:21
 * 描述： 通知适配器
 * 来源：
 */
class NotifyAdapter(data: List<NotifyModel>?) : BaseQuickAdapter<NotifyModel, BaseViewHolder>(R.layout.item_notify, data) {

    override fun convert(helper: BaseViewHolder, item: NotifyModel) {
        helper.setText(R.id.tv_title, item.title)
        helper.setText(R.id.tv_time, item.time)
        helper.setText(R.id.tv_type, item.type)
        helper.setText(R.id.tv_look, item.look)
        helper.addOnClickListener(R.id.tv_edit)
        helper.addOnClickListener(R.id.tv_delete)
    }
}
