package fule.com.kotlindemo.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import fule.com.kotlindemo.view.OnItemClick
import fule.com.kotlindemo.R
import fule.com.kotlindemo.view.MyHolder

/**
 * 作者： njb
 * 时间： 2018/7/16 0016-上午 10:03
 * 描述： 我的
 * 来源：
 */
class MyAdapter : RecyclerView.Adapter<MyHolder>() {
    var list: List<String>  = ArrayList()
    var  click: OnItemClick? =null

    fun add(list: List<String>){
        this.list = list
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        holder.textView.text = list!![position]
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        val view = LayoutInflater.from(parent!!.context).inflate(R.layout.item_holder_view, parent, false)
        val holder = MyHolder(view, click!!)
        return holder
    }

    fun setItemClick(click: OnItemClick){
        this.click = click
    }



}
