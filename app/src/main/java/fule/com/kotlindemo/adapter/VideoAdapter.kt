package fule.com.kotlindemo.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import fule.com.kotlindemo.R
import fule.com.kotlindemo.model.VideoModel

/**
 * 作者： njb
 * 时间： 2018/8/3 0003-上午 8:45
 * 描述： 视频适配器
 * 来源：
 */
class VideoAdapter(list : List<VideoModel>) : BaseQuickAdapter<VideoModel,BaseViewHolder>(R.layout.item_video,list){
    override fun convert(helper: BaseViewHolder, item: VideoModel) {
        helper.setText(R.id.tv_content,item.title)
        helper.setText(R.id.tv_online_name,item.user_name)
        helper.setText(R.id.tv_online_count,item.cillect_number)
    }

}