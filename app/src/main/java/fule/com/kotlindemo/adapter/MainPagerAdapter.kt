package fule.com.kotlindemo.adapter

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.view.View
import android.widget.ImageView
import android.widget.TextView

import fule.com.kotlindemo.R
import fule.com.kotlindemo.fragment.*
import fule.com.kotlindemo.util.Utils


/**
 * Created by njb on 2017/08/02.
 */
class MainPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
    private var tabNames: Array<String>? = null

    override fun getItem(position: Int): Fragment? {
        when (position) {
            0 -> return HomeFragment()
            1 -> return LiveFragment()
            2 -> return CircleFragment()
            3 -> return MineFragment()
        }
        return null
    }

    override fun getCount(): Int {
        return 4
    }

    fun getTabView(context: Context, position: Int): View {
        if (null == tabNames) {
            tabNames = context.resources.getStringArray(R.array.tabs)
        }
        val tabView = View.inflate(context, R.layout.tab_indicator, null)
        val textView = Utils.getView<TextView>(tabView, R.id.tab_title)
        textView.text = tabNames!![position]
        val resId = if (position < tabIcons.size) tabIcons[position] else R.mipmap.ic_launcher
        val imageView = Utils.getView<ImageView>(tabView, R.id.tab_icon)
        imageView.setImageResource(resId)
        return tabView
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return if (position < 0 || position >= tabNames!!.size) {
            super.getPageTitle(position)
        } else tabNames!![position]
    }

    companion object {
        private val tabIcons = intArrayOf(R.mipmap.ic_launcher, R.mipmap.ic_launcher, R.mipmap.ic_launcher, R.mipmap.ic_launcher)
    }
}
