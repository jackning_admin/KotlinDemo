package fule.com.kotlindemo.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import fule.com.kotlindemo.R
import fule.com.kotlindemo.model.FollowModel

/**
 * 作者： njb
 * 时间： 2018/8/2 0002-上午 11:25
 * 描述： 关注适配器
 * 来源：
 */
class FollowAdapter( list: ArrayList<FollowModel>) : BaseQuickAdapter<FollowModel,BaseViewHolder>(R.layout.item_follow,list){
    override fun convert(helper: BaseViewHolder, item: FollowModel) {
        helper.setText(R.id.tv_name,item.user_name)
        helper.setText(R.id.tv_works,item.works)
        helper.setText(R.id.tv_fans,item.fan)
    }

}