package fule.com.kotlindemo.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import fule.com.kotlindemo.R
import fule.com.kotlindemo.model.TeacherModel

/**
 * 作者： njb
 * 时间： 2018/7/26 0026-下午 12:53
 * 描述： 讲师适配器
 * 来源：
 */
class TeacherAdapter(data: List<TeacherModel>?) : BaseQuickAdapter<TeacherModel,BaseViewHolder>(R.layout.item_teacher,data){
    override fun convert(helper: BaseViewHolder, item: TeacherModel) {
        helper.setText(R.id.tv_name,item.name)
        helper.setText(R.id.tv_level,item.level)
        helper.setText(R.id.tv_content,item.content)
        helper.setText(R.id.tv_type,item.type)
    }

}
