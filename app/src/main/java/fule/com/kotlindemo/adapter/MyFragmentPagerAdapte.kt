package fule.com.kotlindemo.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

import fule.com.kotlindemo.fragment.CircleFragment
import fule.com.kotlindemo.fragment.HomeFragment
import fule.com.kotlindemo.fragment.LiveFragment
import fule.com.kotlindemo.fragment.MineFragment

class MyFragmentPagerAdapte(fm: FragmentManager) : FragmentPagerAdapter(fm) {
    private val mTitles = arrayOf("首页", "创视频", "创友圈", "我的")

    override fun getItem(position: Int): Fragment {
        return when (position) {
            1 -> HomeFragment()
            2 -> LiveFragment()
            3 -> CircleFragment()
            else -> MineFragment()
        }
    }

    override fun getCount(): Int {
        return mTitles.size
    }

    //用来设置tab的标题
    override fun getPageTitle(position: Int): CharSequence? {
        return mTitles[position]
    }


}
