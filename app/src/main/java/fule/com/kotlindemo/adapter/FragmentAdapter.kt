package fule.com.kotlindemo.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

import java.util.ArrayList

/**
 * Fargment适配类
 * Created by njb on 2016/2/26.
 */
class FragmentAdapter(fm: FragmentManager,
    private val list: ArrayList<Fragment>) : FragmentPagerAdapter(fm) {
    private val titles = arrayOf("首页", "创视频", "创友圈", "我的")


    override fun getItem(position: Int): Fragment {
        return list[position]
    }

    override fun getCount(): Int {
        return list.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return titles[position]
    }
}
