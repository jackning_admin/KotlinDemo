package fule.com.kotlindemo.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import fule.com.kotlindemo.R
import fule.com.kotlindemo.model.CircleModel

/**
 * 作者： njb
 * 时间： 2018/7/25 0025-下午 1:51
 * 描述： 创友圈适配器
 * 来源：
 */
class CircleAdapter(data:List<CircleModel> ?) : BaseQuickAdapter<CircleModel, BaseViewHolder>(R.layout.item_circle,data) {
    override fun convert(helper: BaseViewHolder, item: CircleModel) {
        helper.setText(R.id.tv_name,item.name)
        helper.setText(R.id.tv_content,item.content)
        helper.setText(R.id.tv_level,item.level)
    }

}
