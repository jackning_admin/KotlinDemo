package fule.com.kotlindemo.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

/**
 * 作者： njb
 * 时间： 2018/5/22 0022-下午 3:56
 * 描述：
 * 来源：
 */
class ViewPagerAdapter(fm: FragmentManager, private val list: List<Fragment>?) : FragmentPagerAdapter(fm) {
    private val tabTitles = arrayOf( "推荐", "关注","产品","活动")

    override fun getItem(position: Int): Fragment {

        return list!![position]
    }


    override fun getCount(): Int {
        return list?.size ?: 0
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return tabTitles[position]
    }
}
