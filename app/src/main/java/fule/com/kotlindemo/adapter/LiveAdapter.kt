package fule.com.kotlindemo.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder

import fule.com.kotlindemo.R
import fule.com.kotlindemo.model.LiveModel

/**
 * 作者： njb
 * 时间： 2018/7/20 0020-上午 11:32
 * 描述： 活动适配器
 * 来源：
 */
class LiveAdapter(data: List<LiveModel>) : BaseQuickAdapter<LiveModel, BaseViewHolder>(R.layout.item_live, data) {

    override fun convert(helper: BaseViewHolder, item: LiveModel) {
        helper.setText(R.id.tv_online_name, item.title)
        helper.setText(R.id.tv_online_count, item.number)
        helper.setText(R.id.tv_onlive_type,item.status)
    }
}
