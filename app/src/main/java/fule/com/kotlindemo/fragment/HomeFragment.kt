package fule.com.kotlindemo.fragment

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.view.View
import fule.com.kotlindemo.R
import fule.com.kotlindemo.adapter.TeacherAdapter
import fule.com.kotlindemo.adapter.ViewPagerAdapter
import fule.com.kotlindemo.base.BaseFragment
import fule.com.kotlindemo.base.BasePresenter
import fule.com.kotlindemo.base.BaseView
import fule.com.kotlindemo.model.TeacherModel
import kotlinx.android.synthetic.main.fm_home.*

/**
 * 作者： njb
 * 时间： 2018/7/18 0018-上午 8:40
 * 描述： 首页
 * 来源：
 */
class HomeFragment : BaseFragment<BasePresenter<BaseView>>() {
    private val schoolFragment = SchoolFragment()
    private val liveFragment = LiveFragment()
    private var list: MutableList<Fragment>? = null
    private var vpAdapter: ViewPagerAdapter? = null

    override fun createPresenter(): BasePresenter<BaseView> {
        return BasePresenter(this)
    }

    override fun loadData() {

    }

    override fun getLayoutId(): Int {
        return R.layout.fm_home
    }

    override fun initView() {

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initFragment()

    }

    private fun initFragment() {
        list = ArrayList()
        list!!.add(schoolFragment)
        list!!.add(liveFragment)

        vpAdapter = ViewPagerAdapter(activity!!.supportFragmentManager, list)
        viewpager.adapter = vpAdapter
        viewpager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        tabLayout!!.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(viewpager))

    }




    override fun addListener() {
    }

    interface OnFragmentInteractionListener {
        fun onFragmentInteraction()
    }

    /**
     * 单例
     */
    companion object {
        fun newInstance(): HomeFragment {
            return HomeFragment()
        }
    }

}