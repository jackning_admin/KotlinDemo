package fule.com.kotlindemo.fragment

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import fule.com.kotlindemo.R
import fule.com.kotlindemo.adapter.PublicAdapter
import fule.com.kotlindemo.base.BaseFragment
import fule.com.kotlindemo.base.BasePresenter
import fule.com.kotlindemo.base.BaseView
import fule.com.kotlindemo.model.PublicModel
import kotlinx.android.synthetic.main.activity_base_toolbar_dark.*
import kotlinx.android.synthetic.main.fm_class.*
import kotlinx.android.synthetic.main.fm_live.*

/**
 * 作者： njb
 * 时间： 2018/8/3 0003-下午 1:49
 * 描述： 公开课
 * 来源：
 */
class PublicClassFragment : BaseFragment<BasePresenter<BaseView>>(){
    private var publicAdapter = PublicAdapter(null)
    private var data = ArrayList<PublicModel>()
    private var publicModel = PublicModel()

    override fun getLayoutId(): Int {
        return R.layout.fm_class
    }

    override fun loadData() {
    }

    override fun initView() {
        if(isViewCreated){

        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar.title = "公开课"
        initAdapter()
    }

    private fun initAdapter() {
        for(i in 1..4){
            publicModel.title = "名医面对面会诊接口各种疑难杂病"
            publicModel.looknumber = "观看205次"
            publicModel.status = "免费"
            data.add(publicModel)
        }
        publicAdapter = PublicAdapter(data)
        val manager = LinearLayoutManager(context)

        rv_class.layoutManager = manager
        rv_class.adapter = publicAdapter
    }

    override fun addListener() {
    }

    override fun createPresenter(): BasePresenter<BaseView> {
        return BasePresenter(this)
    }

}