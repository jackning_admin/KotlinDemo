package fule.com.kotlindemo.fragment

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.view.View
import fule.com.kotlindemo.R
import fule.com.kotlindemo.activity.FansActivity
import fule.com.kotlindemo.activity.FollowActivity
import fule.com.kotlindemo.activity.UserInfoActivity
import fule.com.kotlindemo.adapter.ViewPagerAdapter
import fule.com.kotlindemo.base.BaseFragment
import fule.com.kotlindemo.base.BasePresenter
import fule.com.kotlindemo.base.BaseView
import kotlinx.android.synthetic.main.fm_mine.*

/**
 * 作者： njb
 * 时间： 2018/7/17 0017-下午 5:31
 * 描述： 我的
 * 来源：
 */
class MineFragment : BaseFragment<BasePresenter<BaseView>>(),View.OnClickListener{
    private val recommendFragment = LiveFragment()
    private val followFragment = FollowFragment()
    private val productFragment = ProductFragment()
    private val activeFragment = CircleFragment()
    private var vpAdapter: ViewPagerAdapter? = null
    private var list: MutableList<Fragment>? = null

    override fun loadData() {

    }

    override fun getLayoutId(): Int {
        return R.layout.fm_mine
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tvFllow.setOnClickListener(this)
        tvFans.setOnClickListener(this)
        tv_user_info.setOnClickListener(this)
        initFragment()
    }

    private fun initFragment() {
        list = ArrayList()
        list!!.add(recommendFragment)
        list!!.add(followFragment)
        list!!.add(productFragment)
        list!!.add(activeFragment)

        vpAdapter = ViewPagerAdapter(activity!!.supportFragmentManager, list)
        vpager.adapter = vpAdapter
        vpager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tablayouts))
        tablayouts!!.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(vpager))
    }

    override fun initView() {

    }

    override fun addListener() {
    }

    override fun createPresenter(): BasePresenter<BaseView> {
        return BasePresenter(this)
    }

    /**
     * 单例
     */
    companion object {
        fun newInstance(): MineFragment {
            return MineFragment()
        }
    }

    override fun onClick(v: View?) {
       when(v?.id){
           R.id.tvFans ->
               startActivity(Intent(context,FansActivity::class.java))
           R.id.tvFllow ->
               startActivity(Intent(context,FollowActivity::class.java))
           R.id.tv_user_info ->
               startActivity(Intent(context,UserInfoActivity::class.java))
       }
    }



}