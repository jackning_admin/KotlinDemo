package fule.com.kotlindemo.fragment

import fule.com.kotlindemo.R
import fule.com.kotlindemo.base.BaseFragment
import fule.com.kotlindemo.base.BasePresenter
import fule.com.kotlindemo.base.BaseView

/**
 * 作者： njb
 * 时间： 2018/8/2 0002-下午 5:54
 * 描述： 用户信息
 * 来源：
 */
class UserInfoFragment : BaseFragment<BasePresenter<BaseView>>(){
    override fun getLayoutId(): Int {
        return R.layout.fm_user_msg
    }

    override fun loadData() {
    }

    override fun initView() {
    }

    override fun addListener() {
    }

    override fun createPresenter(): BasePresenter<BaseView> {
        return BasePresenter(this)
    }

}