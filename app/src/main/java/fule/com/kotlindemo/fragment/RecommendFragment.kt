package fule.com.kotlindemo.fragment

import android.app.ActivityOptions
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.view.View
import fule.com.kotlindemo.R
import fule.com.kotlindemo.activity.KotlinActivity
import fule.com.kotlindemo.adapter.VideoAdapter
import fule.com.kotlindemo.base.BaseFragment
import fule.com.kotlindemo.base.BasePresenter
import fule.com.kotlindemo.base.BaseView
import fule.com.kotlindemo.model.VideoModel
import fule.com.kotlindemo.weight.GridSpacingItemDecoration
import kotlinx.android.synthetic.main.fm_activity.*

/**
 * 作者： njb
 * 时间： 2018/8/3 0003-下午 5:53
 * 描述： 推荐
 * 来源：
 */
class RecommendFragment : BaseFragment<BasePresenter<BaseView>>() {
    private var videoAdapter: VideoAdapter? = null
    private var data = ArrayList<VideoModel>()
    private var liveModel: VideoModel? = null

    override fun getLayoutId(): Int {
        return R.layout.fm_activity
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAdapter()
    }

    private fun initAdapter() {
        liveModel = VideoModel()
        for (i in 1..5) {
            liveModel!!.cillect_number = "88"
            liveModel!!.title = "小亲新喷雾"
            liveModel!!.user_name  = "一叶障目"
            data!!.add(liveModel!!)
        }
        videoAdapter = VideoAdapter(data)
        /*   val manager = LinearLayoutManager(MyApplication.getContext())
           rv_live!!.layoutManager = manager*/
        rv_activity.layoutManager = GridLayoutManager(context,2)
        rv_activity.addItemDecoration(GridSpacingItemDecoration(2, 2, true))
        rv_activity!!.adapter = videoAdapter


        videoAdapter!!.setOnItemClickListener { adapter, view, position ->
            val intent = Intent(context, KotlinActivity::class.java)
            var activityOptions = ActivityOptions.makeScaleUpAnimation(view, 0, 0, view.width, view.height)
            startActivity(intent,activityOptions.toBundle())
        }
    }


    override fun loadData() {
    }

    override fun initView() {
    }

    override fun addListener() {
    }

    override fun createPresenter(): BasePresenter<BaseView> {
        return BasePresenter(this)
    }

}