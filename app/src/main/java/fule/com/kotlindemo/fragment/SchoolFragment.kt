package fule.com.kotlindemo.fragment

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import fule.com.kotlindemo.R
import fule.com.kotlindemo.activity.*
import fule.com.kotlindemo.adapter.TeacherAdapter
import fule.com.kotlindemo.base.BaseFragment
import fule.com.kotlindemo.base.BasePresenter
import fule.com.kotlindemo.base.BaseView
import fule.com.kotlindemo.model.TeacherModel
import fule.com.kotlindemo.util.GlideImageLoader
import fule.com.kotlindemo.weight.DividerDecoration
import kotlinx.android.synthetic.main.fm_school.*


/**
 * 作者： njb
 * 时间： 2018/7/17 0017-下午 3:25
 * 描述： 商学院
 * 来源：
 */
class SchoolFragment: BaseFragment<BasePresenter<BaseView>>(),View.OnClickListener {


    private var teacherAdapter: TeacherAdapter? = null
    private var teacherModel: TeacherModel? = null
    private var data = ArrayList<TeacherModel>()



    override fun createPresenter(): BasePresenter<BaseView> {
        return BasePresenter(this)
    }

    override fun loadData() {
    }

    override fun getLayoutId(): Int {
        return R.layout.fm_school
    }

    override fun initView() {
        if(isViewCreated){

        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAdapter()
        initBanner()
        iniListener()
    }

    private fun iniListener() {
        tv_teacher.setOnClickListener(this)
        tv_public.setOnClickListener(this)
        tv_compulsory.setOnClickListener(this)
        tv_exclusive.setOnClickListener(this)
    }

    private fun initBanner() {
        val list = java.util.ArrayList<Int>()
        list.add(R.mipmap.b1)
        list.add(R.mipmap.b2)
        list.add(R.mipmap.b3)
        banner_home.setImages(list)
                .setImageLoader(GlideImageLoader())
                .start()
    }

    private fun initAdapter() {
        teacherModel = TeacherModel()
        for (i in 1..8) {
            teacherModel!!.name = "红太狼"
            teacherModel!!.content = "站在高处，重新理解财富的秘密不是"
            teacherModel!!.level = "新手"
            teacherModel!!.userImg = ""
            data.add(teacherModel!!)
        }
        teacherAdapter = TeacherAdapter(data)
        val layoutManager = LinearLayoutManager(context)
        rv_teacher!!.addItemDecoration(DividerDecoration(context!!.resources.getColor(R.color.ce7e7e7), 1))
        rv_teacher!!.layoutManager = layoutManager
        rv_teacher!!.adapter = teacherAdapter
    }




    override fun addListener() {

    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.tv_teacher ->
                startActivity(Intent(context, TeacherActivity::class.java))
            R.id.tv_public ->
                startActivity(Intent(context,PublicClassActivity::class.java))
            R.id.tv_compulsory ->
                startActivity(Intent(context,CompulsoryActivity::class.java))
            R.id.tv_exclusive ->
                startActivity(Intent(context,ExclusiveActivity::class.java))
        }
    }

}