package fule.com.kotlindemo.fragment

import android.app.ActivityOptions
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.View
import fule.com.kotlindemo.R
import fule.com.kotlindemo.activity.KotlinActivity
import fule.com.kotlindemo.activity.NotifyActivity
import fule.com.kotlindemo.adapter.LiveAdapter
import fule.com.kotlindemo.adapter.ViewPagerAdapter
import fule.com.kotlindemo.base.BaseFragment
import fule.com.kotlindemo.base.BasePresenter
import fule.com.kotlindemo.base.BaseView
import fule.com.kotlindemo.model.LiveModel
import fule.com.kotlindemo.weight.GridSpacingItemDecoration
import kotlinx.android.synthetic.main.fm_activity.*
import kotlinx.android.synthetic.main.fm_live.*

/**
 * 作者： njb
 * 时间： 2018/7/18 0018-上午 8:40
 * 描述： 视频
 * 来源：
 */
class LiveFragment : BaseFragment<BasePresenter<BaseView>>() {
    private var liveAdapter: LiveAdapter? = null
    private var data = ArrayList<LiveModel>()
    private var liveModel: LiveModel? = null

    override fun createPresenter(): BasePresenter<BaseView> {
        return BasePresenter(this)
    }

    override fun loadData() {

    }

    override fun getLayoutId(): Int {
        return R.layout.fm_live
    }

    override fun initView() {
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAdapter()
    }

    private fun initAdapter() {
        liveModel = LiveModel()
        for (i in 0..3) {
            liveModel!!.number = "123"
            liveModel!!.title = "健康讲座"
            liveModel!!.status  = "直播中"
            this.data!!.add(liveModel!!)
        }
        liveAdapter = LiveAdapter(data)
/*        val manager = LinearLayoutManager(MyApplication.getContext())
        rv_live!!.layoutManager = manager*/
        rv_live.layoutManager = GridLayoutManager(context,2)
        rv_live.addItemDecoration(GridSpacingItemDecoration(2, 2, true))
        rv_live!!.adapter = liveAdapter


        liveAdapter!!.setOnItemClickListener { adapter, view, position ->
            val intent = Intent(context, NotifyActivity::class.java)
            var activityOptions = ActivityOptions.makeScaleUpAnimation(view, 0, 0, view.width, view.height)
            startActivity(intent,activityOptions.toBundle())
        }
    }

    override fun addListener() {
    }

    /**
     * 单例
     */
    companion object {
        fun newInstance(): LiveFragment {
            return LiveFragment()
        }
    }
}