package fule.com.kotlindemo.fragment

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import fule.com.kotlindemo.R
import fule.com.kotlindemo.adapter.CircleAdapter
import fule.com.kotlindemo.base.BaseFragment
import fule.com.kotlindemo.base.BasePresenter
import fule.com.kotlindemo.base.BaseView
import fule.com.kotlindemo.model.CircleModel
import kotlinx.android.synthetic.main.fm_circle.*

/**
 * 作者： njb
 * 时间： 2018/7/18 0018-上午 8:40
 * 描述： 创友圈
 * 来源：
 */
class CircleFragment : BaseFragment<BasePresenter<BaseView>>() {
    private var circleAdapter:CircleAdapter? = null
    private var data = ArrayList<CircleModel>()
    private var circleModel:CircleModel? = null


    override fun createPresenter(): BasePresenter<BaseView> {
        return BasePresenter(this)
    }

    override fun loadData() {

    }

    override fun getLayoutId(): Int {
        return R.layout.fm_circle
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAdapter()
    }

    private fun initAdapter() {
        circleModel = CircleModel()
        for(i in 1..8){
            circleModel!!.name = "红太狼"
            circleModel!!.content = "今天很热"
            circleModel!!.level = "新手"
            circleModel!!.img = ""
            data.add(circleModel!!)
        }
        circleAdapter = CircleAdapter(data)
        val layoutManager = LinearLayoutManager(context)
        rv_circle!!.layoutManager = layoutManager
        rv_circle!!.adapter = circleAdapter
    }

    override fun initView() {
    }

    override fun addListener() {
    }

    /**
     * 单例
     */
    companion object {
        fun newInstance(): CircleFragment {
            return CircleFragment()
        }
    }

}