package fule.com.kotlindemo.fragment

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.view.View
import fule.com.kotlindemo.R
import fule.com.kotlindemo.adapter.ViewPagerAdapter
import fule.com.kotlindemo.base.BaseFragment
import fule.com.kotlindemo.base.BasePresenter
import fule.com.kotlindemo.base.BaseView
import kotlinx.android.synthetic.main.fm_video.*

/**
 * 作者： njb
 * 时间： 2018/8/2 0002-下午 4:30
 * 描述： 视频
 * 来源：
 */
class VideoFragment : BaseFragment<BasePresenter<BaseView>>(){
    private val recommendFragment = RecommendFragment()
    private val followFragment = FollowFragment()
    private val productFragment = ProductFragment()
    private val activeFragment = ActiveFragment()
    private var vpAdapter: ViewPagerAdapter? = null
    private var list: MutableList<Fragment>? = null


    override fun getLayoutId(): Int {
        return R.layout.fm_video
    }

    override fun loadData() {
    }

    override fun initView() {
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initFragment()
    }


    private fun initFragment() {
        list = ArrayList()
        list!!.add(recommendFragment)
        list!!.add(followFragment)
        list!!.add(productFragment)
        list!!.add(activeFragment)

        vpAdapter = ViewPagerAdapter(activity!!.supportFragmentManager, list)
        viewPager.adapter = vpAdapter
        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tablayout))
        tablayout!!.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(viewPager))
    }

    override fun addListener() {
    }

    override fun createPresenter(): BasePresenter<BaseView> {
        return BasePresenter(this)
    }

    /**
     * 单例
     */
    companion object {
        fun newInstance(): VideoFragment {
            return VideoFragment()
        }
    }

}