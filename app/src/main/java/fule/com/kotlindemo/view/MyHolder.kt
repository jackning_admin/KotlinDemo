package fule.com.kotlindemo.view

import android.support.v7.widget.RecyclerView
import android.util.SparseArray
import android.view.View
import android.widget.TextView
import fule.com.kotlindemo.R

@Suppress("DEPRECATION")
/**
 * 作者： njb
 * 时间： 2018/7/16 0016-上午 10:02
 * 描述：
 * 来源：
 */
class MyHolder(itemView: View, click: OnItemClick) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

    var textView: TextView

    private var click: OnItemClick? = null

    init {
        textView = itemView.findViewOften(R.id.textview)
        this.click = click
        itemView.setOnClickListener(this)
    }


    private fun <T : View> View.findViewOften(viewId: Int): T {
        var viewHolder: SparseArray<View> = this.tag as? SparseArray<View> ?: SparseArray()
        tag = viewHolder
        var childView: View? = viewHolder.get(viewId)
        if (null == childView) {
            childView = findViewById(viewId)
            viewHolder.put(viewId, childView)
        }
        return childView as T
    }

    override fun onClick(v: View?) {
        if (v != null) {
            click!!.OnItemClick(v, adapterPosition)
        }
    }

}