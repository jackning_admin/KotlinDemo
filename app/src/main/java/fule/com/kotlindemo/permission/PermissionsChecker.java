package fule.com.kotlindemo.permission;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;


import java.util.ArrayList;
import java.util.List;

import fule.com.kotlindemo.R;


/**
 * Created by zhangming on 2016/11/07.
 */

public class PermissionsChecker implements IPermissionsChecker {
    private static final int RequestCode = 10086;
    private static final String EXTRA_PERMISSIONS = "EXTRA_PERMISSIONS";
    private static final String PACKAGE_URL_SCHEME = "package:";
    private final Activity mActivity;
    private OnPermissionsResultListener onPermissionsResultListener;

    public PermissionsChecker(Activity activity) {
        mActivity = activity;
    }

    @Override
    public String[] lacksPermissions(String... permissions) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return null;
        }
        List<String> list = null;
        for (String permission : permissions) {
            if (lacksPermission(permission)) {
                if (null == list) {
                    list = new ArrayList<>();
                }
                list.add(permission);
            }
        }
        if (null != list) {
            String[] array = new String[list.size()];
            list.toArray(array);
            return array;
        }
        return null;
    }

    @Override
    public void requestPermissions(OnPermissionsResultListener listener, Activity activity, String... permissions) {
        onPermissionsResultListener = listener;
        ActivityCompat.requestPermissions(activity, permissions, RequestCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == RequestCode) {
            if (hasAllPermissionsGranted(grantResults)) {
                if (null != onPermissionsResultListener) {
                    onPermissionsResultListener.onGranted();
                }
            } else {
                if (null != onPermissionsResultListener) {
                    onPermissionsResultListener.onDenied();
                }
            }
        }
    }

    @Override
    public void startAppSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.setData(Uri.parse(PACKAGE_URL_SCHEME + mActivity.getPackageName()));
        mActivity.startActivity(intent);
    }


    @Override
    public void showMissingPermissionDialog(@Nullable DialogInterface.OnClickListener negativeListener, @Nullable DialogInterface.OnClickListener positiveListener) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mActivity);
        alertDialog.setTitle(R.string.permission_help);
        alertDialog.setMessage(R.string.permission_help_text);

        if (null != negativeListener) {
            alertDialog.setNegativeButton(R.string.permission_cancel, negativeListener);
        }
        alertDialog.setPositiveButton(R.string.permission_setting, positiveListener);

        alertDialog.show();
    }

    @Override
    public void showMissingPermissionDialog(@Nullable DialogInterface.OnClickListener negativeListener) {
        showMissingPermissionDialog(negativeListener, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startAppSettings();
                mActivity.finish();
            }
        });
    }

    private boolean lacksPermission(String permission) {
        return PackageManager.PERMISSION_DENIED == ContextCompat.checkSelfPermission(mActivity, permission);
    }

    private boolean hasAllPermissionsGranted(@NonNull int[] grantResults) {
        for (int grantResult : grantResults) {
            if (grantResult == PackageManager.PERMISSION_DENIED) {
                return false;
            }
        }
        return true;
    }

    public interface OnPermissionsResultListener {
        void onGranted();

        void onDenied();
    }
}