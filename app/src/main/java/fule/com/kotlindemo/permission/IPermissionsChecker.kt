package fule.com.kotlindemo.permission

import android.app.Activity
import android.content.DialogInterface

/**
 * Created by ZhangMing on 2017/07/13.
 */

interface IPermissionsChecker {
    fun lacksPermissions(vararg permissions: String): Array<String>

    fun requestPermissions(listener: PermissionsChecker.OnPermissionsResultListener, activity: Activity, vararg permissions: String)

    fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray)

    fun showMissingPermissionDialog(negativeListener: DialogInterface.OnClickListener?, positiveListener: DialogInterface.OnClickListener?)

    fun showMissingPermissionDialog(negativeListener: DialogInterface.OnClickListener?)

    fun startAppSettings()
}
