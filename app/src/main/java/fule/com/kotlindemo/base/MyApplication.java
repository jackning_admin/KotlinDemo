package fule.com.kotlindemo.base;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;



/**
 * 作者： ch
 * 时间： 2018/5/8 0008-下午 4:37
 * 描述：
 * 来源：
 */


public class MyApplication extends Application {
    //全局Context
    private static Context sContext;
    private static MyApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        //设置APP应用字体不缩放
        initconfig();

    }


    private void initconfig() {
        Resources res = super.getResources();
        Configuration config = new Configuration();
        config.setToDefaults();
        res.updateConfiguration(config, res.getDisplayMetrics());
    }



    //单例模式中获取唯一的MyApplication实例
    public static MyApplication getInstance() {
        return instance;
    }


    public static Context getContext() {
        return sContext;
    }
}
