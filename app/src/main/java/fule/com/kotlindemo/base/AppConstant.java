package fule.com.kotlindemo.base;

/**
 * Created by ch
 * on 2017/12/22.17:46
 * 作用：
 */

public class AppConstant {
    public static final String LEDAYOUXUAN = "ledayouxuan";
    public static final String APP_KEY = "159357456";
    public static final String BASE_SERVER_URL = "";
       public static final String BASE_WEB_URL = "";
    //public static final String BASE_WEB_URL = "";

        //public static final String BASE_SERVER_URL = "";
         //public static final String BASE_WEB_URL = "";
    //    //商品图片基础路径
    public static String BASE_URL_GOOD = "";
    //文章图片基础路径
    public static String BASE_URL_ARTICLE = "";
    //客服电话
    public static String CUSTOMER_TEL = "4006029038";


    //我的订单
    public static final String MY_ORDER = BASE_WEB_URL + "older_list.html";
    //银行账户
    public static final String BANK_ACCOUNT = BASE_WEB_URL + "bank_account.html";
    //电子币充值
    public static final String ELECTRONIC_RECHAGE = BASE_WEB_URL + "ele_pay_in.html";
    //电子币汇款
    public static final String ELECTRONIC_REMIT = BASE_WEB_URL + "ele_tansfer.html";
    //电子币转账
    public static final String ELECTRONIC_PAYMENT = BASE_WEB_URL + "user_zhuanzhang.html";
    //电子币
    public static final String ELECTRONIC = BASE_WEB_URL + "user_electronic_coin.html";
    //辅销币充值
    public static final String SUBSIDIARY_RECHAGE = BASE_WEB_URL + "fxb_pay_in.html";
    //辅销币汇款
    public static final String SUBSIDIARY_REMIT = BASE_WEB_URL + "fxb_transfer.html";
    //辅销币
    public static final String SUBSIDIARY = BASE_WEB_URL + "user_electronic_coin.html";


    //转账记录
    public static final String PAYMENT_RECORD = BASE_WEB_URL + "transfer_list.html";
    //汇款记录
    public static final String REMITTANCELIST = BASE_WEB_URL + "remittanceList.html";

    //防伪查询
    public static final String SECURITY_CHECK = BASE_WEB_URL + "user_anti.html";
    //用户升级
    public static final String LEVEL_UP = BASE_WEB_URL + "turn_in_leveUp.html";

    //访客
    public static final String VISITOR_MANAGER = BASE_WEB_URL + "user_visitors.html";

    //荣誉
    public static final String HONOR = BASE_WEB_URL + "honor.html";
    //媒体
    public static final String MEDIA = BASE_WEB_URL + "found_media.html";
    //案例
    public static final String CASE = BASE_WEB_URL + "case.html";
    //登录
    public static final String LOGIN = BASE_WEB_URL + "login.html";

    //文章详情url
    public static final String ARTLIST_DETAILS = BASE_WEB_URL + "details.html";

    //充值记录
    public static final String RECHARGE_LIST = BASE_WEB_URL + "rechargeList.html";
    //完成评论
    public static final String THANKS_EVALUATE = BASE_WEB_URL + "thanks_evaluate.html";
    //订单搜索
    public static final String ORDER_LIST_SEARCH = BASE_WEB_URL + "older_list_search.html";

    //商品分享url
    public static final String GOODS_SHARE = BASE_WEB_URL + "goods_share.html";
    //注册
    public static final String REGISTER = BASE_WEB_URL + "register.html";
    //用户升级
    public static final String USER_ACTIVATE = BASE_WEB_URL + "user_activate.html";
    //关于我们
    public static final String USER_AGREEMENT = BASE_WEB_URL + "user_agreement.html";


}
