package fule.com.kotlindemo.base

class BaseModel<T> : Model() {
    var data: T? = null
}