package fule.com.kotlindemo.base

class BaseListModel<T> : Model() {
    var data: List<T>? = null
}