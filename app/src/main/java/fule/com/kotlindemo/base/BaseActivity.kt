package fule.com.kotlindemo.base

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import android.widget.Toast
import butterknife.ButterKnife
import butterknife.Unbinder
import fule.com.kotlindemo.R
import fule.com.kotlindemo.permission.PermissionsChecker
import fule.com.kotlindemo.weight.LoadingDialog

/**
 * 基类 ch
 *
 * @param <P>
</P> */
abstract class BaseActivity<P : BasePresenter<*>> : AppCompatActivity(), BaseView {
    lateinit var context: Context
    var toast: Toast? = null
    protected var presenter: P? = null
    private var dialog: LoadingDialog? = null
    protected var unbinder: Unbinder? = null
    private var permissionsChecker: PermissionsChecker? = null

    protected abstract val layoutId: Int

    protected abstract fun createPresenter(): P

    protected abstract fun initView()

    protected abstract fun addListener()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStatusBar()
        if (layoutId != 0) {
            setContentView(layoutId)
        }
        unbinder = ButterKnife.bind(this)
        context = this
        presenter = createPresenter()
        initView()
        addListener()
    }

    protected fun setStatusBar() {

    }


    /**
     * 打开Activity
     *
     * @param cls
     */
    fun startA(cls: Class<*>) {
        if (cls == null) {
            return
        }

        val intent = Intent(context, cls)
        startActivity(intent)
    }

    override fun onResume() {
        super.onResume()
    }

    public override fun onPause() {
        super.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (presenter != null) {
            presenter!!.detachView()
        }
        if (unbinder != null) {
            unbinder!!.unbind()
        }

    }

    /**
     * @param s
     */
    fun showtoast(s: String) {
        Toast.makeText(context, s, Toast.LENGTH_SHORT).show()
    }


    /**
     * 显示加载动画
     */
    fun showLoadingDialog() {

        if (dialog != null && dialog!!.isShowing) {
            return
        }
        if (null == dialog) {
            dialog = LoadingDialog(context, R.style.CustomDialog)
        }
        dialog!!.setCancelable(false)
        dialog!!.show()

    }

    /**
     * 隐藏加载动画
     */
    fun closeLoadingDialog() {

        if (dialog != null && dialog!!.isShowing) {
            dialog!!.dismiss()
        }
    }


    fun showFileDialog() {}

    fun hideFileDialog() {}


    /**
     * 通过资源res获得view
     *
     * @param res
     * @return
     */
    fun getViewByRes(@LayoutRes res: Int): View {
        return LayoutInflater.from(context).inflate(res, null)
    }

    /**
     * 获得TextView 的文本
     *
     * @param tv
     * @return
     */
    fun getTV(tv: TextView?): String {
        return tv?.text?.toString()?.trim { it <= ' ' } ?: ""
    }

    override fun showLoading() {
        showLoadingDialog()
    }

    override fun hideLoading() {
        closeLoadingDialog()
    }

    override fun showError(msg: String) {
        showtoast(msg)
    }

    override fun onErrorCode(model: Model) {
        if (model.code == 103 || model.code == 400) {
            showtoast(model.commen!!)
        } else if (model.code == 101) {
            //登录失效/未登录
            showtoast(model.commen!!)
            //清除保存的用户信息

        }
    }

    override fun showLoadingFileDialog() {
        showFileDialog()
    }

    override fun hideLoadingFileDialog() {
        hideFileDialog()
    }

    override fun onProgress(totalSize: Long, downSize: Long) {}

    fun addFragmentToActivity(fragmentManager: FragmentManager,
                              fragment: Fragment, frameId: Int) {
        val transaction = fragmentManager.beginTransaction()
        if (!fragment.isAdded)
            transaction.add(frameId, fragment)
        fragmentManager.fragments.filter { it.id == fragment.id }.map { transaction.hide(it) }
        transaction.show(fragment)
        transaction.commit()
    }

}
