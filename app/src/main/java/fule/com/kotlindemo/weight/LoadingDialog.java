package fule.com.kotlindemo.weight;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.WindowManager;


import fule.com.kotlindemo.R;

/**
 * 作者： njb
 * 时间： 2018/7/13 0013-上午 8:24
 * 描述：
 * 来源：
 */
public class LoadingDialog extends ProgressDialog {

    public LoadingDialog(Context context)
    {
        super(context);
    }

    public LoadingDialog(Context context, int theme)
    {
        super(context, theme);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        init(getContext());
    }
    private void init(Context context)
    {
        //设置不可取消，点击其他区域不能取消，实际中可以抽出去封装供外包设置
        setCancelable(false);
        setCanceledOnTouchOutside(false);

        setContentView(R.layout.dialog_loading);
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.WRAP_CONTENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getWindow().setAttributes(params);
    }
    @Override
    public void show()
    {
        super.show();
    }
}
