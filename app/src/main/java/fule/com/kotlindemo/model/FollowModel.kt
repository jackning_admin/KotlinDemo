package fule.com.kotlindemo.model

import java.io.Serializable

/**
 * 作者： njb
 * 时间： 2018/8/2 0002-上午 11:25
 * 描述： 关注实体类
 * 来源：
 */
class FollowModel : Serializable{
    var user_img: String? = null
    var user_name: String?= null
    var works:String?= null
    var fan:String?= null
}