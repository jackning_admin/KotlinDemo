package fule.com.kotlindemo.model

import java.io.Serializable

/**
 * 作者： njb
 * 时间： 2018/8/3 0003-上午 8:47
 * 描述： 视频实体类
 * 来源：
 */
class VideoModel : Serializable{
    var title: String? = null
    var user_name: String? = null
    var cillect_number : String? = null
}