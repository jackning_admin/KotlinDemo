package fule.com.kotlindemo.model;

import java.io.Serializable;

/**
 * 作者： njb
 * 时间： 2018/8/14 0014-上午 10:28
 * 描述： 个人
 * 来源：
 */
public class Person implements Serializable{
    private String name;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
