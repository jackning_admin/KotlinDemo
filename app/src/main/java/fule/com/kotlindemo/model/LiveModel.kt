package fule.com.kotlindemo.model

import java.io.Serializable

/**
 * 作者： njb
 * 时间： 2018/7/17 0017-下午 4:28
 * 描述： 活动实体类
 * 来源：
 */
class LiveModel : Serializable{
    var title: String? = null
    var number: String? = null
    var status:String? = null
}