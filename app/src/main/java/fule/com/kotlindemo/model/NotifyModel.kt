package fule.com.kotlindemo.model

import java.io.Serializable

/**
 * 作者： njb
 * 时间： 2018/8/8 0008-下午 4:22
 * 描述： 通知实体类
 * 来源：
 */
class NotifyModel : Serializable {
    var title: String? = null
    var type: String? = null
    var look: String? = null
    var time: String? = null
}
