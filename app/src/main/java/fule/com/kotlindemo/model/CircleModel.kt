package fule.com.kotlindemo.model

import java.io.Serializable

/**
 * 作者： njb
 * 时间： 2018/7/25 0025-下午 1:52
 * 描述： 创友圈实体类
 * 来源：
 */
class CircleModel : Serializable{
    var photo:String? = null
    var name:String? = null
    var level:String? = null
    var content:String? = null
    var img:String? = null
}