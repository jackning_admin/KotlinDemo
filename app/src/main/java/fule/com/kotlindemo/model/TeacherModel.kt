package fule.com.kotlindemo.model

import java.io.Serializable

/**
 * 作者： njb
 * 时间： 2018/7/26 0026-下午 12:53
 * 描述： 讲师实体类
 * 来源：
 */
class TeacherModel : Serializable{
    var userImg:String? = null
    var name:String? = null
    var level:String? = null
    var content:String? = null
    var type:String? = null
}