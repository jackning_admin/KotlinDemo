package fule.com.kotlindemo.model

import java.io.Serializable

/**
 * 作者： njb
 * 时间： 2018/8/3 0003-上午 11:57
 * 描述： 公开课实体类
 * 来源：
 */
class PublicModel : Serializable{
    var username:String? =  null
    var userimg:String? = null
    var title:String? = null
    var looknumber:String? = null
    var status:String? = null
}